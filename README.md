# 각 위치 링크

[**팀 초기위치로 이동**](https://bitbucket.org/maplego)  


[**트렐로 보드로 이동**](https://bitbucket.org/maplego/figure-inside/addon/trello/trello-board)  

[**팀 위키로 이동**](https://bitbucket.org/maplego/figure-inside/wiki/Home)

---

# 이 프로젝트는 무엇인가요?

심심할때 만드는 피규어 예약구매 알림 서비스 프로젝트

---

# 이 저장소는 무엇인가요?

이 저장소는 메인 프로젝트 공간으로, 기획 및 이슈관리등이 이루어지는 곳이다.  
이 공간에 실제로 코드가 올라가지는 않는다.

---

### 쓰고싶은 말을 자유롭게 Edit 해서 추가하자. 문법은 Markdown 이다.